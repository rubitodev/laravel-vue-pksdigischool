<?php
require_once("animal.php");
require_once("frog.php");
require_once("ape.php");
$animal = new Animal("shaun");
echo "Name : " . $animal->name . "<br>"; // "shaun"
echo "Legs : " . $animal->legs . "<br>"; // 4
echo "Cold blooded : " . $animal->cold_blooded . "<br>"; // "no"

echo "<br>";
$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>";
echo "legs : " . $kodok->legs . "<br>";
echo "Cold blooded : " . $kodok->cold_blooded . "<br>"; // "no"
echo $kodok->jump();
echo "<br><br>";

$sungokong = new Kera("kera sakti");
echo "Name : " . $sungokong->name . "<br>";
echo "legs : " . $sungokong->legs . "<br>";
echo "Cold blooded : " . $sungokong->cold_blooded . "<br>"; // "no"
echo $sungokong->Yell();
